# Accounting 

This General Accounting project has been used for 4 years in a company for transactions such as day-book, 
Trial balance, Ledger, Budget, Period On / Off etc. 
The code is written on Linux Slackware 10.2(kernel 2.4) with the programming language "Progress v9". 
New versions of Progress can be applied by modifying "Progress | OpenEdge".

Bu Genel Muhasebe projesi Yevmiye, Mizan, Defteri Kebir, Bütçe, Dönem Açma/Kapama
vb işlemleri için bir firmada 4 yıl boyunca kullanılmıştır. 
Kod "Progress v9" programlama dili ile Linux Slackware 10.2 (Kernel 2.4) üzerinde yazılmıştır. 
Progress in yeni sürümleri "Progress|OpenEdge" de değişiklik yapılarak uygulanabilir.

_Emrullah İLÇİN_

- [butgel.p](/accounting/butgel.p)
- [butgid.p](/accounting/butgid.p)
- [butgir.p](/accounting/butgir.p)
- [comp.bat](/accounting/comp.bat)
- [copy.bat](/accounting/copy.bat)
- [hesgir.p](/accounting/hesgir.p)
- [heslis.p](/accounting/heslis.p)
- [heslis.prn](/accounting/heslis.prn)
- [hesprn.p](/accounting/hesprn.p)
- [hessil.p](/accounting/hessil.p)
- [kebir.prn](/accounting/kebir.prn)
- [keblis.p](/accounting/keblis.p)
- [kebmua.p](/accounting/kebmua.p)
- [kebmuae.p](/accounting/kebmuae.p)
- [kurtar.bat](/accounting/kurtar.bat)
- [m.df](/accounting/m.df)
- [mizack.p](/accounting/mizack.p)
- [mizack.prn](/accounting/mizack.prn)
- [mizacke.p](/accounting/mizacke.p)
- [mizalt.p](/accounting/mizalt.p)
- [mizan.prn](/accounting/mizan.prn)
- [mizcar.p](/accounting/mizcar.p)
- [mizgen.p](/accounting/mizgen.p)
- [mode_lpt1.bat](/accounting/mode_lpt1.bat)
- [muha-dump.bat](/accounting/muha-dump.bat)
- [muha.df](/accounting/muha.df)
- [muhack.d](/accounting/muhack.d)
- [muhack.df](/accounting/muhack.df)
- [muhmnu.p](/accounting/muhmnu.p)
- [muhmulti.bat](/accounting/muhmulti.bat)
- [muhpla.d](/accounting/muhpla.d)
- [muhpla.df](/accounting/muhpla.df)
- [muhprm.d](/accounting/muhprm.d)
- [muhprm.df](/accounting/muhprm.df)
- [muhserv.bat](/accounting/muhserv.bat)
- [muhsingle.bat](/accounting/muhsingle.bat)
- [muhyev.d](/accounting/muhyev.d)
- [muhyev.df](/accounting/muhyev.df)
- [procore](/accounting/procore)
- [r-help.p](/accounting/r-help.p)
- [run.bat](/accounting/run.bat)
- [yarayl.p](/accounting/yarayl.p)
- [yarbas.p](/accounting/yarbas.p)
- [yarfir.p](/accounting/yarfir.p)
- [yarsif.p](/accounting/yarsif.p)
- [yarson.d](/accounting/yarson.d)
- [yarson.df](/accounting/yarson.df)
- [yarson.p](/accounting/yarson.p)
- [yevba.p](/accounting/yevba.p)
- [yevdok.prn](/accounting/yevdok.prn)
- [yevduz.p](/accounting/yevduz.p)
- [yevkan.p](/accounting/yevkan.p)
- [yevknt.p](/accounting/yevknt.p)
- [yevlis.p](/accounting/yevlis.p)
- [yevprn.p](/accounting/yevprn.p)
- [yevsil.p](/accounting/yevsil.p)
- [yevyni.p](/accounting/yevyni.p)
- [yedek.bat](/accounting/yedek.bat)


