/* BUTGID.P  */
/* Yazan Emrullah ILCIN emr@uludag.edu.tr 1996 Upgrade 2000 */
/* butce gider listesi verir  */

/*
   onemli aciklama
   04 100 adli hesapa butce gir'de tam degeri girilecek
   30 100 ve 30 000 hesaplarni alt ve ana hesaba ayni deger 2 kere girilecek
   28 100 ve 28 000   "
   32 100 ve 32 000   "
   olacak

    diger hesaplarin alt hesaplarini toplayip ana hesaba koyuyorum
    ama yukarida yazilanlarin alt hesabi olmadigi icin rakkam direkt girilecek

*/

def var ana-top     as dec format "->>>,>>>,>>>,>>>,>>9".
def var alt-top     as dec format "->>>,>>>,>>>,>>>,>>9".
def var ana-top-akt as dec format "->>>,>>>,>>>,>>>,>>9".
def var alt-top-akt as dec format "->>>,>>>,>>>,>>>,>>9".

def var o1-ay-top    as dec format "->>>,>>>,>>>,>>>,>>9".
def var tum1-ay-top  as dec format "->>>,>>>,>>>,>>>,>>9".
def var o2-ay-top    as dec format "->>>,>>>,>>>,>>>,>>9".
def var tum2-ay-top  as dec format "->>>,>>>,>>>,>>>,>>9".
def var o3-ay-top    as dec format "->>>,>>>,>>>,>>>,>>9".
def var tum3-ay-top  as dec format "->>>,>>>,>>>,>>>,>>9".

def var i as dec format ">>>,>>>,>>>,>>>,>>>".

def var gelir-top as dec format "->>>,>>>,>>>,>>>,>>9".
def var gider-top as dec format "->>>,>>>,>>>,>>>,>>9".
def var hangi-ay as  int.
def var basla as int format "99".
def var bitir as int format "99".


def var butce-tam as dec format "->>>,>>>,>>>,>>>,>>9".
def var butce-akt as dec format "->>>,>>>,>>>,>>>,>>9".
def var butce-oay as dec format "->>>,>>>,>>>,>>>,>>9".
def var butce-yil as dec format "->>>,>>>,>>>,>>>,>>9".


def var t-al  as char format "x(10)".
def var t-al0  as char format "x(10)".
def var t-al1 as char format "x(10)".
def var t1900 as int  format "9999".
def var t1901 as int  format "9999".
def var t1902 as int  format "9999".
def var t1903 as int  format "9999".

def var ciz as char format "x(132)".
def var ciz2 as char format "x(132)".


ciz = "------ ------------------------- ---------------- ---------------- ---------------- --------------- --------------- ----------------".
ciz2 = "====== ========================= ================ ================ ================ =============== =============== ================".


basla=01. bitir = 99.
update  "Hangi Ayın Listesi :" hangi-ay format ">>" auto-return skip
	"Başlangiç Hesabı   :" basla     skip
	"Bitiş Hesabı       :" bitir
  with frame a no-underline  row 10 centered  no-label
  title "Gider Bütçesi".
hide all no-pause.
/*
if opsys = "msdos" then output to printer page-size 155 paged.
		   else
		   output to butce.prn paged page-size 155.
*/

output to printer page-size 155 paged.

hide all no-pause.
  t1900 = int(substring(string(today),7,2)) + 1900  .
  t1901 = int(substring(string(today),7,2)) + 1900  .
  t1902 = int(substring(string(today),7,2)) + 1900  .
  t1903 = int(substring(string(today),7,2)) + 1900  .

if hangi-ay = 1  then do: t-al = "      OCAK". T-AL1 = "     31.01". end.
if hangi-ay = 2  then do: t-al = "     ŞUBAT". T-AL1 = "     28.02". end.
if hangi-ay = 3  then do: t-al = "      MART". T-AL1 = "     31.03". end.
if hangi-ay = 4  then do: t-al = "     NİSAN". T-AL1 = "     30.04". end.
if hangi-ay = 5  then do: t-al = "     MAYIS". T-AL1 = "     31.05". end.
if hangi-ay = 6  then do: t-al = "   HAZİRAN". T-AL1 = "     30.06". end.
if hangi-ay = 7  then do: t-al = "    TEMMUZ". T-AL1 = "     31.07". end.
if hangi-ay = 8  then do: t-al = "   AGUSTOS". T-AL1 = "     31.08". end.
if hangi-ay = 9  then do: t-al = "     EYLÜL". T-AL1 = "     30.09". end.
if hangi-ay = 10 then do: t-al = "      EKİM". T-AL1 = "     31.10". end.
if hangi-ay = 11 then do: t-al = "     KASIM". T-AL1 = "     30.11". end.
if hangi-ay = 12 then do: t-al = "    ARALIK". T-AL1 = "     31.12". end.
t-al0 = t-al.
tum1-ay-top = 0.
tum2-ay-top = 0.
tum3-ay-top = 0.
disp
     t-al t1900      "AYI KARŞILAŞTIRMALI GİDER BÜTÇESİ"  space(65)
			     page-number ".sayfa" skip(1)
     "Fasıl                           "
     "           " t1901  "      AKTARMA EK"
     "           NİHAİ"   t-al0 t1903
      t-al1 t1902  "                " SKIP
	"No.... Hesap Adı................"
	"         BÜTÇESİ" "    TAHSİSATLARI"
	"           BÜTÇE" "      HARCAMASI"
	"      HARCAMASI"  "          BAKİYE"
	with frame ilkbas no-label no-box width 132 page-top.
ana-top = 0.
alt-top = 0.
ana-top-akt = 0.
alt-top-akt = 0.
/* ----- */


for each muhpla
	      where p-alt1 >=  0 and p-alt2 >= 0 and  p-butce-gider > 0
	      by p-ana descending.
     /*   disp p-ana p-alt1 "X".    */

     /*
     disp p-adi p-ana p-alt1 p-alt2 p-butce-gider recid(muhpla).
     */
	  /* ---- alttaki guruplari toplama almiyorum  */
	  /* --- cunki bu hesaplari topltarak buluyorum */

/* ----- GAYRIMENKUL=ana gurubu ve SUURETIM,PERSONEL=alt gurubu harici -- */
/* ------toplamlari alir ---- GERCEK HESAPLARI  ------------------------- */

	  if p-alt1 = 0 or
	     ( p-ana = 02 and p-alt1 = 180 ) or
	 /*  ( p-ana = 04 and p-alt1 = 100 ) or  */
	     ( p-ana = 04 and p-alt1 = 200 ) or
	     ( p-ana = 04 and p-alt1 = 300 ) or
	     ( p-ana = 04 and p-alt1 = 400 ) or
	     ( p-ana = 08 and p-alt1 = 100 )
	 /*  ( p-ana = 28 and p-alt1 = 100 ) or */
	 /*  ( p-ana = 30 and p-alt1 = 100 ) or */
	 /*  ( p-ana = 30 and p-alt1 = 200 )    */
	  /* ( p-ana = 32 and p-alt1 = 100 ) */ then do: end. else
	     do:
	     /*
	     disp p-ana p-alt1 ana-top alt-top.
	     */
	     ana-top     = ana-top     + p-butce-gider.
	     alt-top     = alt-top     + p-butce-gider.
	     ana-top-akt = ana-top-akt + p-butce-gider-aktar.
	     alt-top-akt = alt-top-akt + p-butce-gider-aktar.
	     end.

/* --------- p-alt1 0 olanlar toplanir --(GAYRIMENKUL Satiri)------ */
       if p-alt1 = 0 then
	 do:
	 /* disp "T" p-ana p-alt1 ana-top .   */


	 p-butce-gider        = ana-top.
	 p-butce-gider-aktar  = ana-top-akt.

	 ana-top = 0.
	 ana-top-akt = 0.
	 alt-top = 0.
	 alt-top-akt = 0.
	 end.
/* ------p-alt1 180 200 vb ise (SU URETIM, PERSONEL, SAİR GİDER--------  */

	  if ( p-ana = 02 and p-alt1 = 180 and p-alt2 = 0 ) or
	  /*   ( p-ana = 04 and p-alt1 = 100 and p-alt2 = 0 ) or  */
	     ( p-ana = 04 and p-alt1 = 200 and p-alt2 = 0 ) or
	     ( p-ana = 04 and p-alt1 = 300 and p-alt2 = 0 ) or
	     ( p-ana = 04 and p-alt1 = 400 and p-alt2 = 0 ) or
	     ( p-ana = 08 and p-alt1 = 100 and p-alt2 = 0 )
	  /*   ( p-ana = 28 and p-alt1 = 100 and p-alt2 = 0 ) or  */
	  /* ( p-ana = 30 and p-alt1 = 100 and p-alt2 = 0 ) or  */
	  /* ( p-ana = 30 and p-alt1 = 200 )                    */
	  /*  ( p-ana = 32 and p-alt1 = 100  ) */    then
	    do:
	    /* disp "*" p-ana p-alt1 alt-top .  */
	    p-butce-gider        = alt-top.
	    p-butce-gider-aktar  = alt-top-akt.
	    alt-top = 0.
	    alt-top-akt = 0.
	    end.


end. /* for each muhpla end  */






/* ================================================================ */

/*  ----- ACIKLAMA ------------- */
/*  muhpla daki p-alacak'a tum yilin toplamlari toplaniyor */
/*  muhpla daki p-borc 'a o aydaki toplamlar toplaniyor olarak kullanildi */


for each muhpla
	     where p-alt1 >= 0 and p-alt2 >= 0 and  p-butce-gider > 0
	      by p-ana descending.


/* --------- muhpla altindakti 2.dongu  ------------------------ */
for each muhyev use-index kebir-index where
      yana = p-ana  and yalt1 = p-alt1 and p-alt2 = yalt2
      /* alttaki devreden hesaplar toplama alinmiyor */
      and  ( ( any1 <> 1 and yana <> 30 and yalt1 <> 100 )
	  or ( any1 <> 1 and yana <> 08 and yalt1 <> 101 )
	  or ( any1 <> 1 and yana <> 08 and yalt1 <> 102 )
	  or ( any1 <> 1 and yana <> 08 and yalt1 <> 103 )
	  or ( any1 <> 1 and yana <> 08 and yalt1 <> 104 )
	  or ( any1 <> 1 and yana <> 28 and yalt1 <> 100 ) ).


/* ---------- YIL TOPLAMI BASI (1)------------------- */

/* ----- GAYRIMENKUL=ana gurubu ve SUURETIM,PERSONEL=alt gurubu harici -- */
/* ------toplamlari alir ---- GERCEK HESAPLARI  ------------------------- */

	  if p-alt1 = 0 or
	     ( p-ana = 02 and p-alt1 = 180 ) or
	     ( p-ana = 02 and p-alt1 = 100 ) or
	     ( p-ana = 04 and p-alt1 = 200 ) or
	     ( p-ana = 04 and p-alt1 = 300 ) or
	     ( p-ana = 04 and p-alt1 = 400 ) or
	     ( p-ana = 08 and p-alt1 = 100 ) or
	     ( p-ana = 28 and p-alt1 = 100 ) or
	     ( p-ana = 30 and p-alt1 = 100 ) or
	     ( p-ana = 30 and p-alt1 = 200 ) or
	     ( p-ana = 32 and p-alt1 = 100 ) then
	     do:
	     end. else
	     do:
	       /*
	       if
	      /* alttaki devreden hesaplar toplama alinmiyor */
		  ( yana <> 1 and yana <> 30 and yalt1 <> 100 )
	      and ( yana <> 1 and yana <> 08 and yalt1 <> 101 )
	      and ( yana <> 1 and yana <> 08 and yalt1 <> 102 )
	      and ( yana <> 1 and yana <> 08 and yalt1 <> 103 )
	      and ( yana <> 1 and yana <> 08 and yalt1 <> 104 )
	      and ( yana <> 1 and yana <> 28 and yalt1 <> 100 ).
	      */
	     tum1-ay-top = tum1-ay-top  + borc.    /*o aydaki ler toplanir */
	     tum2-ay-top = tum2-ay-top  + borc.    /*o aydaki ler toplanir */
	     tum3-ay-top = tum3-ay-top  + borc.    /*o aydaki ler toplanir */
	     end.
      /* ----------- YIL TOPLAMI SONU --!1)------- */

/* ----------- o ay harcamalari Toplami BASI (1)----------- */

  if hangi-ay = dayi then
  DO:   /* BUYUK İÇ DONGU BASİ */
/* ----- GAYRIMENKUL=ana gurubu ve SUURETIM,PERSONEL=alt gurubu harici -- */
/* ------toplamlari alir ---- GERCEK HESAPLARI  ------------------------- */

	  if p-alt1 = 0 or
	     ( p-ana = 02 and p-alt1 = 180 ) or
	     ( p-ana = 02 and p-alt1 = 100 ) or
	     ( p-ana = 04 and p-alt1 = 200 ) or
	     ( p-ana = 04 and p-alt1 = 300 ) or
	     ( p-ana = 04 and p-alt1 = 400 ) or
	     ( p-ana = 08 and p-alt1 = 100 ) or
	     ( p-ana = 28 and p-alt1 = 100 ) or
	     ( p-ana = 30 and p-alt1 = 100 ) or
	     ( p-ana = 30 and p-alt1 = 100 ) or
	     ( p-ana = 32 and p-alt1 = 100 ) then
	     do:
	     end. else
	     do:
	     o1-ay-top = o1-ay-top  + borc.    /*o aydaki ler toplanir */
	     o2-ay-top = o2-ay-top  + borc.    /*o aydaki ler toplanir */
	     o3-ay-top = o3-ay-top  + borc.    /*o aydaki ler toplanir */
	     end.
       END. /* ---------BUYUK İÇ DONGU SONU  (ayi toplami) ------ */

    /* ------------- a ay harcamalari sonu ---(1) ---------- x */


    end. /* muhyev endi */



/* -------------- YIL TOPAMI BASI---(2)------------ */

/* --------- p-alt1 0 olanlar toplanir --(GAYRIMENKUL Satiri)------ */
       if p-alt1 = 0 then
	 do:
	 assign p-alacak = tum1-ay-top.
	 tum1-ay-top = 0.
	 tum2-ay-top = 0.
	 tum3-ay-top = 0.
	 end.


/* ------y-alt1 180 200 vb ise (SU URETIM, PERSONEL, SAİR GİDER--------  */

	  if ( p-ana = 02 and p-alt1 = 180 and p-alt2 = 0 ) or
	     ( p-ana = 04 and p-alt1 = 100 and p-alt2 = 0 ) or
	     ( p-ana = 04 and p-alt1 = 200 and p-alt2 = 0 ) or
	     ( p-ana = 04 and p-alt1 = 300 and p-alt2 = 0 ) or
	     ( p-ana = 04 and p-alt1 = 400 and p-alt2 = 0 ) or
	     ( p-ana = 08 and p-alt1 = 100 and p-alt2 = 0 ) or
	     ( p-ana = 28 and p-alt1 = 100 and p-alt2 = 0 ) or
	     ( p-ana = 30 and p-alt1 = 100 and p-alt2 = 0 ) or
	     ( p-ana = 30 and p-alt1 = 200 and p-alt2 = 0 ) or
	     ( p-ana = 32 and p-alt1 = 100 and p-alt2 = 0 ) then
	    do:
	    assign p-alacak    = tum2-ay-top.
	    tum2-ay-top = 0.
	    end.
   /* ---------  ana va alt hesaplari haricindekileri yazar */
	  if p-alt1 = 0 or
	     ( p-ana = 02 and p-alt1 = 180 ) or
	     ( p-ana = 02 and p-alt1 = 100 ) or
	     ( p-ana = 04 and p-alt1 = 200 ) or
	     ( p-ana = 04 and p-alt1 = 300 ) or
	     ( p-ana = 04 and p-alt1 = 400 ) or
	     ( p-ana = 08 and p-alt1 = 100 ) or
	     ( p-ana = 28 and p-alt1 = 100 ) or
	     ( p-ana = 30 and p-alt1 = 100 ) or
	     ( p-ana = 30 and p-alt1 = 200 ) or
	     ( p-ana = 32 and p-alt1 = 100 ) then do: end. else
	     do:
	     assign p-alacak = tum3-ay-top.
	     tum3-ay-top = 0.
	     end.


/* ---------------YIL TOPLAMI SONU ---(2)---------------- */


/* --------------- o AY toplami basi ---(2) ------------ */

/* --------- p,y-alt1 0 olanlar toplanir --(GAYRIMENKUL Satiri)------ */
       if p-alt1 = 0 then
	 do:
	 assign p-borc = o1-ay-top.
	 o1-ay-top = 0.
	 o2-ay-top = 0.
	 o3-ay-top = 0.
	 end.

/* ------y-alt1 180 200 vb ise (SU URETIM, PERSONEL, SAİR GİDER--------  */

	  if ( p-ana = 02 and p-alt1 = 180 and p-alt2 = 0 ) or
	     ( p-ana = 04 and p-alt1 = 100 and p-alt2 = 0 ) or
	     ( p-ana = 04 and p-alt1 = 200 and p-alt2 = 0 ) or
	     ( p-ana = 04 and p-alt1 = 300 and p-alt2 = 0 ) or
	     ( p-ana = 04 and p-alt1 = 400 and p-alt2 = 0 ) or
	     ( p-ana = 08 and p-alt1 = 100 and p-alt2 = 0 ) or
	     ( p-ana = 28 and p-alt1 = 100 and p-alt2 = 0 ) or
	     ( p-ana = 30 and p-alt1 = 100 and p-alt2 = 0 ) or
	     ( p-ana = 30 and p-alt1 = 200 and p-alt2 = 0 ) or
	     ( p-ana = 32 and p-alt1 = 100 and p-alt2 = 0 )    then
	    do:
	    assign p-borc    = o2-ay-top.
	    o2-ay-top = 0.
	    end.
   /* ---------  ana va alt hesaplari haricindekileri yazar */
	  if p-alt1 = 0 or
	     ( p-ana = 02 and p-alt1 = 180 ) or
	     ( p-ana = 02 and p-alt1 = 100 ) or
	     ( p-ana = 04 and p-alt1 = 200 ) or
	     ( p-ana = 04 and p-alt1 = 300 ) or
	     ( p-ana = 04 and p-alt1 = 400 ) or
	     ( p-ana = 08 and p-alt1 = 100 ) or
	     ( p-ana = 28 and p-alt1 = 100 ) or
	     ( p-ana = 30 and p-alt1 = 100 ) or
	     ( p-ana = 30 and p-alt1 = 200 ) or
	     ( p-ana = 32 and p-alt1 = 100 ) then do: end. else
	     do:
	     assign p-borc = o3-ay-top.
	     o3-ay-top = 0.
	     end.


   END.  /* muhpla  DONGUSU SONU  */

/* ----------- o ayin harcama toplami sonu ------------- */


for each muhpla where p-butce-gider > 0
	  /* bu alt hesaplar printerden gorulsun istenmiyor */
       and ( (p-ana  <> 28 and p-alt1 <> 100 ) or
	   ( p-ana  <> 32 and p-alt1 <> 100 ) ).

if p-alt1 = 0  then
     do:
     butce-tam = butce-tam + p-butce-gider.
     butce-akt = butce-akt + p-butce-gider-aktar.
     butce-oay = butce-oay + p-borc.
     butce-yil = butce-yil + p-alacak.
     end.
if p-alt1 = 0  then  disp ciz2  with frame x16  no-label no-box width 132.

 display  p-ana p-alt1 p-adi format "X(26)"
   /* 1 */  p-butce-gider         format ">>>,>>>,>>>,>>9"
   /* 2 */  p-butce-gider-aktar   format "->>>,>>>,>>>,>>9"
   /* 3 */  ( p-butce-gider + p-butce-gider-aktar ) format "->>>,>>>,>>>,>>9"
   /* 4 */  p-borc     format ">>>,>>>,>>>,>>9"
   /* 5 */  p-alacak   format ">>>,>>>,>>>,>>9"
   /* 6 */  ( (p-butce-gider + p-butce-gider-aktar ) -  p-alacak )
	     format "->>>,>>>,>>>,>>9" with frame aa3 no-label no-box width 132.
  if ( p-ana = 2 and p-alt1 = 0 ) or
     ( p-ana = 4 and p-alt1 = 0 ) or
     ( p-ana = 8 and p-alt1 = 0 ) or
     ( p-ana = 32 and p-alt1 = 0 )
   then disp ciz with frame aa4 no-label no-box width 132.

end.
/*     display ciz1  skip with frame bb1 no-label no-box width 132. */
     display  "Toplam" "" format "X(26)"
   /* 1 */  butce-tam   format ">>>,>>>,>>>,>>9"
   /* 2 */  butce-akt   format "->>>,>>>,>>>,>>9"
   /* 3 */  ( butce-tam + butce-akt ) format "->>>,>>>,>>>,>>9"
   /* 4 */  butce-oay     format ">>>,>>>,>>>,>>9"
   /* 5 */  butce-yil   format ">>>,>>>,>>>,>>9"
   /* 6 */  ( (butce-tam + butce-akt ) -  butce-yil )
	    format "->>>,>>>,>>>,>>9" with frame bb1 no-label no-box width 132.

     display ciz2   with frame bb12 no-label no-box width 132.

output  close.

/*
 unix cat butce.prn.
*/
