/* YEVPRN.P  */
/* Yazan Emrullah ILCIN emr@uludag.edu.tr 1996 Upgrade 2000 */
def var i as int.
def var j as int.
def var sonu as int format ">>>>>>>".
def var basi as int format ">>>>>>>".
def var topl as char format "x(75)".
def var   borc-top as dec format ">>>,>>>,>>>,>>>,>>>".
def var alacak-top as dec format ">>>,>>>,>>>,>>>,>>>".
def var   borc-genel as dec format ">>>,>>>,>>>,>>>,>>>".
def var alacak-genel as dec format ">>>,>>>,>>>,>>>,>>>".
def var sec as char format "x" .
def var siraliEH as char  format "x".
for first muhack.
end.
set "başlangıç nosu............:" basi skip
    "bitiş nosu ...............:"       sonu   skip
    "Girişe göremi listelensin .E/H..:" siraliEH
	 validate(siraliEH = "E" or siraliEH = "e"
	  or siraliEH = "H" or siraliEH = "h" , "hatali secim  ")
	   with frame a
	   centered row 9 no-labels title "yevmiye listesi çekimi".
hide all no-pause.
message "Lütfen biraz bekleyiniz...".
borc-genel = 0.
alacak-genel = 0.
if opsys = "msdos" then output to printer page-size 60.
		    else output to yevdok.prn page-size 60.

if siraliEH = "E" or siraliEH = "e" then
  do:
  for each  muhack where  a-any1 > basi - 1  and a-any1 <  sonu + 1.
   topl =  string(a-any1,"99999")  + ")---------------------------" +
	  string(a-gun,"99,") + string(a-ayi,"99,") + string(a-yil,"9999") +
	"--------------------------------".
    disp topl      skip with frame disp1 no-label no-box .
    borc-top = 0.
    alacak-top = 0.


  for each muhyev where any1 = a-any1 and any2 NE 0
       and ( borc > 0 or alacak > 0 ).

  find muhpla where

	     p-ana = yana and p-alt1 = yalt1 and  p-alt2 = yalt2 no-error.

   topl =
    string(yana,"999 ") + string(yalt1,">>> ")  + string(yalt2,">>>>>").


 if borc > 0 then
  topl =  topl + " " +
	       string(p-adi,"x(24)") + string(borc,">>>,>>>,>>>,>>>,>>>").
 if  alacak > 0 then
  topl = "      " + topl + " " + string(p-adi,"x(36)") +
	      string(alacak,">>>,>>>,>>>,>>>,>>>").


    display any2 format ">>>9" topl
		    with frame br1 no-label no-box 17  down.

		      /*
    if acik > "" then disp acik with frame br1
		      no-label no-box no-hide down .

      */

	borc-top = borc-top + borc.
	alacak-top = alacak-top + alacak.

	borc-genel = borc-genel + borc.
	alacak-genel = alacak-genel + alacak.

     end. /* muhyev */
/*     hide all no-pause.     */

     topl = string(a-gun,"99,") + string(a-ayi,"99,") + string(a-yil,"9999,").
     disp topl format "x(10)"
		  "/" a-any1 FORMAT "99999"
		  "TAR NO.LU FİŞ MUCİBİ" skip
		  with frame son1 no-label no-box down  row 19 overlay.

     display
     "---------------------------------------------------------"   +
	"-----------------------" skip
	   with frame son1 no-label no-box down .
     display   space(21) "Fiş Toplamı :      "  borc-top  alacak-top   skip
			       with frame son1 .

     pause .
     hide frame son1 no-pause.
  end. /* muhack */
     hide all no-pause.
      display space(21) "Genel Toplam :   "  borc-genel alacak-genel skip
			     with frame son11if  no-box no-label.

   END. /* siraliEH  nin endi  */

/* ---------------------------- */

    ELSE

/* -------------------------------- */

   do:   /* siraliEH nin "H" si   */

  for each  muhack where  a-any1 >   basi - 1  and a-any1 <  sonu + 1.
   topl =  string(a-any1,"99999")  + ")---------------------------" +
	  string(a-gun,"99,") + string(a-ayi,"99,") + string(a-yil,"9999") +
				 "------------------------------------".
    disp topl      skip with frame disp1E no-label no-box .
    borc-top = 0.
    alacak-top = 0.


  for each muhyev use-index yevsira
	      where any1 = a-any1 and any2 > 0 and yana > 0
	      and (borc > 0 or  alacak > 0 ).
  find muhpla where

	     p-ana = yana and p-alt1 = yalt1 and  p-alt2 = yalt2 no-error.

   topl =
    string(yana,"999 ") + string(yalt1,">>> ")  + string(yalt2,">>>>>").


 if borc > 0 then
  topl =  topl + " " +
	       string(p-adi,"x(24)") + string(borc,">>>,>>>,>>>,>>>,>>>").
 if  alacak > 0 then
  topl = "      " + topl + " " + string(p-adi,"x(36)") +
	      string(alacak,">>>,>>>,>>>,>>>,>>>").


    display any2 format ">>>9" topl
		    with frame br1E no-label no-box 17  down.

		      /*
    if acik > "" then disp acik with frame br1
		      no-label no-box no-hide down .

      */

	borc-top = borc-top + borc.
	alacak-top = alacak-top + alacak.

	borc-genel = borc-genel + borc.
	alacak-genel = alacak-genel + alacak.

     end. /* muhyev */
/*     hide all no-pause.     */

     topl = string(a-gun,"99,") + string(a-ayi,"99,") + string(a-yil,"9999,").
     disp topl format "x(10)"
		  "/" a-any1 FORMAT "99999"
		  "TAR NO.LU FİŞ MUCİBİ" skip
		  with frame son1E no-label no-box down  row 19 overlay.

     display
     "---------------------------------------------------------"   +
	"-----------------------" skip
	   with frame son1e no-label no-box down .
     display   space(21) "Fiş Toplamı :      "  borc-top  alacak-top   skip
			       with frame son1e .

     pause .
     hide frame son1 no-pause.
  end. /* muhack */
     hide all no-pause.
      display space(21) "Genel Toplam :   "  borc-genel alacak-genel skip
			     with frame son11if  no-box no-label.

    END.  /* siraliEH nin sonu   */

     output close    .
     if opsys = "unix" then
		unix cat yevdok.prn.
