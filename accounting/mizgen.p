/* MIZGEN.P  */
/* Yazan Emrullah ILCIN emr@uludag.edu.tr 1996 Upgrade 2000 */
/* **************** mizan ********************+ */
def var mizan-brc     as dec format ">>>,>>>,>>>,>>>,>>9".
def var mizan-alc     as dec format ">>>,>>>,>>>,>>>,>>9".
def var mizan-brc-bak as dec format ">>>,>>>,>>>,>>>,>>9".
def var mizan-alc-bak as dec format ">>>,>>>,>>>,>>>,>>9".

def var miz-brc-top     as dec format ">>>,>>>,>>>,>>>,>>9".
def var miz-alc-top     as dec format ">>>,>>>,>>>,>>>,>>9".
def var miz-brc-bak-top as dec format ">>>,>>>,>>>,>>>,>>9".
def var miz-alc-bak-top as dec format ">>>,>>>,>>>,>>>,>>9".
def var topl as char format "x(10)".
def var secim as char format "x".
def var fad  as char format "x(40)".
def var miz-tarih as char format "x(10)".
def var ana-sec-bas as int format "99".
def var ana-sec-son as int format "99".
def var bas-ay as int format ">>".
def var bit-ay as int format ">>".
for first muhprm:
fad = firmaadi.
end.
set  "Ana hesab basi.........:" ana-sec-bas  format ">>" skip
     "Ana hesab sonu.........:" ana-sec-son  format ">>" skip
     "Mizan tarihi.(aciklama):" miz-tarih skip
     "Mizan Başlangic ayi....:" bas-ay    skip
     "Mizan Bitiş tarihi.....:" bit-ay
		 with frame ana-s centered no-label row 10
		title "genel mizan listesi".
hide all no-pause.
 if opsys = "msdos" then output to printer page-size 65.
		  else output to mizan.prn page-size 65.
 /*
 topl =       substring(string(today),4,2)  + "."  +
	      substring(string(today),1,2)  + ".19" +
	      substring(string(today),7,2) .
*/
display fad  space(15)
	"***  GENEL MİZAN ***" space(30) "TARİH : " miz-tarih  skip(2)
	     with frame baslik
	     no-label  no-box width 132 .

display "ANA   H E S A P    A D I      " FORMAT "X(50)"
	"      B O R Ç       " +
	"    A L A C A K     " +
	"    BORC BAKİYE     " +
	"   ALACAK BAKİYE    "
	skip with  FRAME C NO-LABEL NO-BOX WIDTH 132.
display "===== ============================================" format "x(50)"
	" ===================" +
	" ===================" +
	" ===================" +
	" ==================="
	skip with frame c no-label no-box width 132.
hide all no-pause.
  /*
  for each muhpla where p-ana > 0 and p-ana < 4 and p-alt1 = 0 and p-alt2 = 0:
  */

    for each muhpla where  p-ana > ana-sec-bas - 1 and p-ana < ana-sec-son + 1
		     and  p-alt1 = 0 and p-alt2 = 0:

    hide all no-pause.

    mizan-brc = 0.       mizan-alc = 0.
    mizan-brc-bak = 0 .  mizan-alc-bak = 0.
    pause 0.


   for each  muhyev    use-index kebir-index
	      where yana = p-ana and
	      ( dayi > bas-ay - 1 and dayi < bit-ay + 1 ).
   if borc   > 0 then   mizan-brc = mizan-brc + borc.
   if alacak > 0 then   mizan-alc = mizan-alc + alacak.
   end.


  if mizan-brc > mizan-alc then mizan-brc-bak = mizan-brc - mizan-alc.
  if mizan-alc > mizan-brc then mizan-alc-bak = mizan-alc - mizan-brc.
  if mizan-brc = mizan-alc then
	       do:
	       mizan-brc-bak = 0.
	       mizan-alc-bak = 0.
	       end.
       display p-ana space(3) p-adi format "x(45)"
	       mizan-brc mizan-alc mizan-brc-bak mizan-alc-bak
	       with frame b23 no-label no-box width 132.
       display
	"----- --------------------------------------------" format "x(50)"
	" -------------------" +
	" -------------------" +
	" -------------------" +
	" -------------------"
	skip with frame b23 no-label no-box width 132.
miz-brc-top = miz-brc-top + mizan-brc.
miz-alc-top = miz-alc-top + mizan-alc.
miz-brc-bak-top = miz-brc-bak-top + mizan-brc-bak.
miz-alc-bak-top = miz-alc-bak-top + mizan-alc-bak.


end.

   display "             GENEL TOPLAM........:"  format "x(51)"
       miz-brc-top miz-alc-top miz-brc-bak-top miz-alc-bak-top
       with frame aa no-label no-box width 132.

display "===== ============================================" format "x(50)"
	" ===================" +
	" ===================" +
	" ===================" +
	" ==================="
	skip with frame aa no-label no-box width 132.
     output  close.
     if opsys = "unix" then unix cat mizan.prn.
     unix cat mizan.prn >> dene | mizgen.prn.
