/* KEBMUA.P  */
/* Yazan Emrullah ILCIN emr@uludag.edu.tr 1996 Upgrade 2000 */

/*    Mavin (kebir defteri )...../  kebmua.p.../  MUAVIN DEFTER  */
def var i as int.
def var j as int.
def var basi as int format ">>>>>>".
def var sonu as int format ">>>>>>>".
def var topl as char format "x(78)".
def var   borc-top as dec format ">>>,>>>,>>>,>>>,>>>".
def var alacak-top as dec format ">>>,>>>,>>>,>>>,>>>".
def var   borc-genel as dec format ">>>,>>>,>>>,>>>,>>>".
def var alacak-genel as dec format ">>>,>>>,>>>,>>>,>>>".
def var sec as char format "x" .
/*
for first muhpla where p-ana = 1.
end.
*/
set "başlangıç nosu: " basi  format "99" skip
    "bitiş nosu    : "       sonu     format "99"
	   with frame a
	   centered row 9 no-labels title "Muavin Defter".
hide all no-pause.

borc-genel = 0.
alacak-genel = 0.
if opsys = "msdos" then output to printer.
		   else
		   output to kebir.prn.

disp space(50) "M U A V İ N   D E F T E R "
	      with frame basilk no-label no-box  width 132.
disp space(50) "==========================" space(2)
	      with frame baslik1 no-label no-box width 132.
  for each  muhpla where
	 p-ana > basi - 1  and muhpla.p-ana <  sonu + 1
	and   p-alt1  > 0:     /* and p-alt2 > 0: */

    borc-top = 0.
    alacak-top = 0.
  disp p-ana p-alt1 "    "  p-adi
       skip(1)  with frame br112 no-label no-box width 132.

  disp "TARİH....   FİŞNO  HESAP  A Ç I K L A M A     " +
       "                                       "   +
       "     B O R Ç             A L A C A K   "
       skip with frame br112 no-label no-box width 132.
  disp "==========  =====  =====  =================== " +
       "                                       "   +
       "=================== ==================="
       skip with frame br112 no-label no-box width 132.

 for each muhyev use-index kebir-index where yana = muhpla.p-ana
	  and p-alt1 = yalt1  and p-alt2 = yalt2
	     and ( borc > 0 or alacak > 0 ).
   topl =
    string(dgun,"99,") + string(dayi,"99,") +
    string(dyil , "9999").

	  display  topl format "x(11)" any1 format "99999 "
		      yalt2  format "99999 "
		   acik format "x(58)"
		    borc format ">>>,>>>,>>>,>>>,>>>"
		    alacak format ">>>,>>>,>>>,>>>,>>>"
		    with frame br1 no-label no-box width 132.


	borc-top = borc-top + borc.
	alacak-top = alacak-top + alacak.

	borc-genel = borc-genel + borc.
	alacak-genel = alacak-genel + alacak.

     end. /* muhyev */
  /*   hide all no-pause.    */

     display
     "-------------" +
     "-------------------------------------------------------"   +
     "-------------------------------------------------------" skip
	   with frame son1 no-label no-box width 132.
     display  space(67) "Cari Toplam :    "  borc-top  alacak-top   skip
		       with frame son1 .

  if borc-top   > alacak-top   then
		      do:
		      borc-top   = borc-top   - alacak-top  .
		      alacak-top   = 0.
		      end.
  if alacak-top   > borc-top   then
		      do:
		      alacak-top   = alacak-top   - borc-top  .
		      borc-top   = 0.
		      end.
  if alacak-top   = borc-top   then
		      do:
		      alacak-top   = 0.
		      borc-top   = 0.
		      end.
      display space(67) "Bakiye           " borc-top   alacak-top   skip(1)
		with frame son123 no-label no-box width 132 .

  /*   pause .    */
  end. /* muhack */
     disp
     "==============" +
     "======================================================="   +
     "=======================================================" skip
	   with frame son11if no-label no-box width 132.
      display space(67) "Genel Toplam :   "  borc-genel alacak-genel skip
		with frame son11gen  no-box no-label width 132.
  if borc-genel > alacak-genel then
		      do:
		      borc-genel = borc-genel - alacak-genel.
		      alacak-genel = 0.
		      end.
  if alacak-genel > borc-genel then
		      do:
		      alacak-genel = alacak-genel - borc-genel.
		      borc-genel = 0.
		      end.
  if alacak-genel = borc-genel then
		      do:
		      alacak-genel = 0.
		      borc-genel = 0.
		      end.

      display space(67) "En son Bakiye    " borc-genel alacak-genel skip
		with frame son11bak no-label no-box width  132.
     disp
     "==============" +
     "======================================================="   +
     "=======================================================" skip
	   with frame son11if no-label no-box width 132.

     output close    .
     if opsys = "unix" then unix cat kebir.prn.
